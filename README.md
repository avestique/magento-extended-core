# README #

Version 0.1.0

This is MAGENTO module which is created for help to developers save them time.
It has some features in admin panel that magento didn't has before. 
At the same time module has some useful source models and helpers.
All features list you can find bellow.

### How do I get set up? ###

* clone repo to your project
* refresh magento cache
* go to system/settings/AVESTIQUE EXTENSIONS/Example Excode to see an example how it's used.

### How to use multiselectbox condition? ###

* create new class which is extended from class Avestique_Extendcore_Block_Adminhtml_System_Config_Form_Field_Select
* add __constructor to new class like that:
```
protected function _construct()
{
  parent::_construct();

  // resource which is implemented from Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface
  
  $source = Mage::getModel('av_excore/adminhtml_system_config_source_example_cars');

  $this->addSelectColumn('columnCode', 'columnLabel', $source , array(
      'size' => 10,
      'style' => '',
      'class' => ''
  ));

  $this->addMultiSelectColumn('columnCode1', 'columnLabel1', $source , array(
      'size' => 10,
      'style' => '',
      'class' => ''
  ));

  $this->setButtonLabel('Add row');
}
```

* add new filed to system.xml
```
<condition_select translate="label comment">
    <label>Selectbox condition example</label>
    <frontend_model>av_excore/adminhtml_system_config_form_field_example_select</frontend_model>
    <backend_model>adminhtml/system_config_backend_serialized_array</backend_model>
    <sort_order>2</sort_order>
    <show_in_default>1</show_in_default>
    <show_in_website>1</show_in_website>
    <show_in_store>1</show_in_store>
    <comment>For using selectbox</comment>
</condition_select>
```

### How to use category filter? ###
* add category filte to system.xml like that:
```
<categories translate="label">
    <label>Categories</label>
    <sort_order>40</sort_order>
    <show_in_default>1</show_in_default>
    <show_in_website>1</show_in_website>
    <show_in_store>1</show_in_store>
    <frontend_type>text</frontend_type>
    <frontend_model>av_excore/adminhtml_system_config_form_field_category</frontend_model>
</categories>
```

### Contacts ###

Please, feel free to contact me on email: developer@avestique.com