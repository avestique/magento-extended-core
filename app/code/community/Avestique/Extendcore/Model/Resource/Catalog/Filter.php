<?php
/**
 * 25.08.16
 * Avestique Developer - v9.magento.com
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     v9.magento.com
 * @copyright   Copyright (c) 2016 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Filter.php
 */

class Avestique_Extendcore_Model_Resource_Catalog_Filter extends Varien_Object
{
    protected $_tableIndex = 'tmp_categories_index';

    /**
     * @param $collection Mage_Catalog_Model_Resource_Product_Collection
     */
    public function initCategories($collection)
    {
        $_categories = array();

        $collection = Mage::getResourceModel('catalog/category_collection')->addNameToResult();
        /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
        foreach ($collection as $category) {
            $structure = preg_split('#/+#', $category->getPath());
            $pathSize  = count($structure);
            if ($pathSize > 1) {
                $path = array();
                for ($i = 1; $i < $pathSize; $i++) {
                    $path[] = $collection->getItemById($structure[$i])->getName();
                }

                $_categories[$category->getId()] = array(
                    'path' => implode('/', $path),
                    'name' => $category->getName(),
                    'level' => $category->getLevel()
                );
            }
        }

        //$connection = Mage::getModel('core/resource')->getConnection(Mage_Core_Model_Resource::DEFAULT_WRITE_RESOURCE);

        $connection = $collection->getConnection();

        $connection->query("
            DROP TABLE IF EXISTS `{$this->_tableIndex}`;

            CREATE TEMPORARY TABLE `{$this->_tableIndex}` (
                `category_id` INT(10) UNSIGNED NOT NULL,
                `level` INT(10) UNSIGNED NOT NULL,
                `name` TEXT DEFAULT NULL,
                `path` TEXT DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        $connection->query("ALTER TABLE `{$this->_tableIndex}` ADD INDEX `TMP_INDEX_CATEGORY_ID` (`category_id`)");
        $connection->query("ALTER TABLE `{$this->_tableIndex}` ADD INDEX `TMP_INDEX_CATEGORY_LEVEL` (`level`)");

        $data = array();

        foreach($_categories as $category_id => $_item)
        {
            $data[] = array(
                'category_id' => $category_id,
                'name'  => $_item['name'],
                'path'  => $_item['path'],
                'level' => $_item['level']
            );

            if (count($data) > 1000)
            {
                $connection->insertMultiple($this->_tableIndex, $data);
                $data = array();
            }
        }

        if (count($data))
        {
            $connection->insertMultiple($this->_tableIndex, $data);
        }
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->_tableIndex;
    }
}