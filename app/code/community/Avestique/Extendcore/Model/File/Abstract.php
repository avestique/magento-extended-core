<?php
/**
 * 27.08.15
 * Avestique Developer - project_relod
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     project_relod
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Abstract.php
 */

abstract class Avestique_Extendcore_Model_File_Abstract extends Varien_Object
{
    protected $_chunkEND = '';

    protected $_file = null;

    abstract function chunkCallback($data, &$lines);

    public function parseFile()
    {
        $fHandle = fopen($this->_file, 'r');

        /** @var $lines - just for debug */
        $lines = 15;

        setlocale(LC_ALL, 'en_US.UTF8');

        if (null != $fHandle) {

            $success = $this->file_get_contents_chunked($this->_file, 16384, function($chunk, &$fHandle, $iteration) use (&$lines)
            {
                /*
                    * Do what you will with the {&chunk} here
                    * {$handle} is passed in case you want to seek
                    * to different parts of the file
                    * {$iteration} is the section fo the file that has been read so
                    * ($i * 4096) is your current offset within the file.
                */

                $previousChunk = $this->_chunkEND;

                file_put_contents(Mage::getBaseDir('var').'/tmp',$previousChunk.$chunk);

                $handle = fopen(Mage::getBaseDir('var').'/tmp', 'r' );
                $size = filesize(Mage::getBaseDir('var').'/tmp');

                $this->_chunkEND = '';

                $data = array();

                while ($line = fgets($handle, $size)) {
                    $data[] = $line;
                    $this->_chunkEND = $line;
                }

                array_pop($data);

                $this->chunkCallback($data, $lines);
            });

            if($success)
            {
                if ($this->_chunkEND) {
                    file_put_contents(Mage::getBaseDir('var') . '/tmp', $this->_chunkEND);

                    $handle = fopen(Mage::getBaseDir('var') . '/tmp', 'r');
                    $size = filesize(Mage::getBaseDir('var') . '/tmp');

                    $this->_chunkEND = '';

                    $data = array();

                    while ($line = fgets($handle, $size)) {
                        //Mage::log(mb_convert_encoding($line,'utf-8'));
                        $data[] = $line;
                    }

                    $this->chunkCallback($data, $lines);
                }
            }
        }

        unlink(Mage::getBaseDir('var') . '/tmp');

        // if (Mage::helper('avestique_sync')->isProfileEnabled())
            Mage::log("Lines in CSV $lines :" . microtime(true), null, 'sync.log');

        return $this;
    }

    public function setFile($file)
    {
        if (!file_exists($file))
            throw new Exception("File $file does not exist.");

        $this->_file = $file;

        return $this;
    }
    
    /**
     * @param $file
     * @param $chunk_size
     * @param $callback
     * @return bool
     */
    protected function file_get_contents_chunked($file, $chunk_size, $callback)
    {
        try
        {
            $handle = fopen($file, "r");

            $i = 0;

            while (!feof($handle))
            {
                call_user_func_array($callback, array(
                    fread($handle,$chunk_size),
                    &$handle,
                    $i
                ));

                $i++;
            }

            fclose($handle);

        }
        catch(Exception $e)
        {
            trigger_error("file_get_contents_chunked::" . $e->getMessage(),E_USER_NOTICE);
            return false;
        }

        return true;
    }
}