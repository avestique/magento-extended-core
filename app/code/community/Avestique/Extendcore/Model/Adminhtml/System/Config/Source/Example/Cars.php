<?php
/**
 * 21.06.15
 * Avestique Developer - ver9
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     ver9
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Cars.php
 */

class Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Example_Cars implements Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface
{
    public function toArray()
    {
        return array(
            'bmw' => 'BMW',
            'fiat' => 'Fiat',
            'nissan' => 'Nissan'
        );
    }
}