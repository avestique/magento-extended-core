<?php
/**
 * 22.06.15
 * Avestique Developer - ver9
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     ver9
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Delivery.php
 */

class Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Delivery
    implements Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface
{
    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $methods = array();
        $shippingMethods = Mage::getSingleton('shipping/config')->getAllCarriers();
        //$methods = array(array('value'=>'', 'label'=>Mage::helper('adminhtml')->__('--Please Select--')));
        foreach ($shippingMethods as $shippingCode=>$paymentModel) {
            $shippingTitle = Mage::getStoreConfig('carriers/'.$shippingCode.'/title') . ': ' . strip_tags(Mage::getStoreConfig('carriers/'.$shippingCode.'/name') ? Mage::getStoreConfig('carriers/'.$shippingCode.'/name') : '');
            $methods[$shippingCode] = $shippingTitle . '(' . $shippingCode . ')';
        }

        return $methods;
    }
}