<?php
/**
 * 19.06.16
 * Avestique Developer - project_relod
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     project_relod
 * @copyright   Copyright (c) 2016 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file StaticPage.php
 */

class Avestique_Extendcore_Model_Adminhtml_System_Config_Source_ProductSets implements Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface
{
    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $entityTypeId = Mage::getModel('eav/entity')
            ->setType('catalog_product')
            ->getTypeId();

        $options = Mage::getModel('eav/entity_attribute_set')->getCollection()->setEntityTypeFilter($entityTypeId);

        $list = array();

        foreach($options as $item)
        {
            $list[$item->getId()] = $item->getAttributeSetName() . ' (' . $item->getAttributeSetId() . ')';
        }

        return $list;
    }
}