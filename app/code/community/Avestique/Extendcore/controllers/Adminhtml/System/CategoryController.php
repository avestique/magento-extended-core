<?php
/**
 * 18.08.16
 * Avestique Developer - project_relod
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     project_relod
 * @copyright   Copyright (c) 2016 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file ${FILE_NAME}
 */

//include_once Mage::getBaseDir('app') . '/code/core/Mage/Adminhtml/controllers/System/ConfigController.php';

class Avestique_Extendcore_Adminhtml_System_CategoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Grid Action
     * Display list of products related to current category
     *
     * @return void
     */
    public function gridCategoriesAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('av_excore/adminhtml_system_config_form_field_category_filter', 'system.filter.category')
                ->setCategories(Mage::app()->getRequest()->getParam('categories', null))
                ->toHtml()
        );
    }
}