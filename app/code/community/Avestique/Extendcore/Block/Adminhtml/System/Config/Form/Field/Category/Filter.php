<?php
/**
 * 18.08.16
 * Avestique Developer - project_relod
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     project_relod
 * @copyright   Copyright (c) 2016 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Filter.php
 */

class Avestique_Extendcore_Block_Adminhtml_System_Config_Form_Field_Category_Filter extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->setId('category_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    public function getMultipleRows($item)
    {
        return null;
    }

    public function getMassactionBlockHtml()
    {
        return '';
    }
    
    public function getRowClickCallback()
    {
        return false;
    }

    /**
     * Prepare collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $filter = $this->getParam($this->getVarNameFilter(), null);

        if (is_string($filter)) {
            $data = $this->helper('adminhtml')->prepareFilterString($filter);
        }

        //$collection = Mage::getResourceModel('catalog/category_collection')->addNameToResult();
        $collection = Mage::getModel('catalog/category')->getCollection()->addNameToResult();

        Mage::getResourceModel('av_excore/catalog_filter')->initCategories($collection);

        if (!empty($data['grid_full_path']) && $data['grid_full_path'])
        {
            $collection->getSelect()->joinInner(array('tmp_category' => Mage::getResourceModel('av_excore/catalog_filter')->getTable()), 'tmp_category.category_id = e.entity_id', array('full_path' => 'tmp_category.path'));
        }
        else
        {
            $collection->getSelect()->joinLeft(array('tmp_category' => Mage::getResourceModel('av_excore/catalog_filter')->getTable()), 'tmp_category.category_id = e.entity_id', array('full_path' => 'tmp_category.path'));
        }

        if (!$this->getCategories() && ($categoryIds = $this->_getCheckedCategories()))
        {
            $collection->addIdFilter($categoryIds);
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Checks when this block is readonly
     *
     * @return boolean
     */
    public function isReadonly()
    {
        return false;
    }

    /**
     * @param $collection Mage_Catalog_Model_Resource_Category_Collection
     * @param $column Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Checkbox
     */
    public function filterGrid($collection, $column)
    {
        $categoryIds = $this->getCategories();

        if (is_array($categoryIds) && count($categoryIds))
        {
            if ($column->getFilter()->getValue()) //
            {
                $collection->addIdFilter($categoryIds);
            }
            else
            {
                $collection->addFieldToFilter('entity_id', array('nin' => $categoryIds));
            }
        }
    }

    /**
     * @param $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection
     * @param $column
     */
    public function filterGridByPath($collection, $column)
    {
        if ($value = $column->getFilter()->getValue()) //
        {

            //$collection->getSelect()->joinInner(array('tmp_category' => Mage::getResourceModel('av_excore/catalog_filter')->getTable()), 'tmp_category.category_id = e.entity_id', array('full_path' => 'tmp_category.path'));

            $collection->getSelect()->where("tmp_category.path like '%$value%'");//addFilter($collection->getConnection()->prepareSqlCondition('tmp_category.path', array('like'=>"%".$value."%")));

            //print $collection->load()->getSelect();
        }
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $values = $this->_getCheckedCategories();

        $this->addColumn('checked_category_entities', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'checked_category_entities',
            'values'            => $values,
            'align'             => 'center',
            'index'             => 'entity_id',
            'filter_condition_callback' => array(
                $this, 'filterGrid'
            )
        ));

        if (!$this->getCategories() && is_array($values) && count($values))
            $this->getColumn('checked_category_entities')->getFilter()->setValue(1);

        $this->addColumn('id', array(
            'header'    => Mage::helper('av_excore')->__('ID'),
            'sortable'  => true,
            'width'     => 20,
            'index'     => 'entity_id',
            'name'      => 'grid_entity_id'
        ));

        $this->addColumn('grid_name', array(
            'header'    => Mage::helper('av_excore')->__('Name'),
            'index'     => 'name',
            'width'     => 180,
            'name'      => 'grid_name'
        ));

        $this->addColumn('grid_level', array(
            'header'    => Mage::helper('av_excore')->__('Level'),
            'width'     => 20,
            'type'      => 'number',
            'index'     => 'level',
            'name'      => 'grid_level'
        ));

        $this->addColumn('grid_full_path', array(
            'header'    => Mage::helper('av_excore')->__('Full path'),
            'index'     => 'full_path',
            //'width'     => 100,
            'name'      => 'grid_full_path',
            'filter_condition_callback' => array(
                $this, 'filterGridByPath'
            )
            //'filter'    => Mage::getBlockSingleton('av_excore/adminhtml_export_filterpath')
        ));

        return parent::_prepareColumns();
    }

    /**
     * Rerieve grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('excore/system_category/gridCategories', array('_current'=>true));

        //return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/gridCategories', array('_current'=>true));
    }

    /**
     * Retrieve selected crosssell products
     *
     * @return array
     */
    protected function _getCheckedCategories()
    {
        $categoryIDs = $this->getCategories();
        if (!is_array($categoryIDs)) {
            $categoryIDs = $this->getCheckedCategories();
        }
        
        return $categoryIDs;
    }

    /**
     * Retrieve crosssell products
     *
     * @return array
     */
    public function getCheckedCategories()
    {
        $categoryIDs = $this->getCategories();

        if (!is_array($categoryIDs)) {

            $categoryIDs = array();

            if ($this->getElement() instanceof Varien_Data_Form_Element_Text && $this->getElement()->getValue())
            {
                $categoryIDs = explode('&', $this->getElement()->getValue());
            }
        }

        return $categoryIDs;
    }

    /**
     * Return row url for js event handlers
     *
     * @param Mage_Catalog_Model_Product|Varien_Object
     * @return string
     */
    public function getRowUrl($item)
    {
        return null;
    }
}