<?php
/**
 * 21.06.15
 * Avestique Developer - ver9
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     ExtentedCore
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Select.php
 */

class Avestique_Extendcore_Block_Adminhtml_System_Config_Form_Field_Example_Select extends Avestique_Extendcore_Block_Adminhtml_System_Config_Form_Field_Select
{
      protected function _construct()
      {
          parent::_construct();

          $source = Mage::getModel('av_excore/adminhtml_system_config_source_example_cars');

          $this->addSelectColumn('columnCode', 'columnLabel', $source , array(
              'size' => 10,
              'style' => '',
              'class' => ''
          ));

          $this->addMultiSelectColumn('columnCode1', 'columnLabel1', $source , array(
              'size' => 10,
              'style' => '',
              'class' => ''
          ));

          $this->setButtonLabel('Add row');
      }
}