<?php
/**
 * 21.06.15
 * Avestique Developer - ver9
 *
 * Created by PhpStorm.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     ExtendedCore
 * @copyright   Copyright (c) 2014 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Select.php
 */

/**
 *
 * Using Example
 *
 * CustomClass extends Avestique_Extendcore_Block_Adminhtml_System_Config_Form_Field_Select
 * {
 *      protected function _construct()
 *      {
 *          parent::_construct();
 *
 *          $this->addSelectColumn('columnCode', 'columnLabel', instance of Avestique_CheckoutDependency_Model_Adminhtml_System_Config_Source_Interface, array(
 *              'size' => 10,
 *              'style' => '',
 *              'class' => ''
 *          ));
 *
 *          $this->addMultiSelectColumn('columnCode', 'columnLabel', instance of Avestique_CheckoutDependency_Model_Adminhtml_System_Config_Source_Interface, array(
 *              'size' => 10,
 *              'style' => '',
 *              'class' => ''
 *          ));
 *
 *          $this->setButtonLabel('Add row');
 *      }
 * }
 *
 * Use it like the system.xml do
 *
 */
class Avestique_Extendcore_Block_Adminhtml_System_Config_Form_Field_Select extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_fieldValues = array();

    protected $_selectBox = array();

    public function __construct()
    {
        parent::__construct();

        $this->setTemplate('avestique/extendedcore/system/config/form/field/select.phtml');
    }

    /**
     * @param $label
     * @return $this
     */
    public function setButtonLabel($label)
    {
        $this->_addButtonLabel = Mage::helper('adminhtml')->__($label);

        return $this;
    }

    /**
     * @param $id
     * @param $label
     * @param Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface $values
     * @param array $params
     * @return $this
     */
    public function addSelectColumn($id, $label, Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface $values, $params = array())
    {
        $options = array(
            'label' => Mage::helper('adminhtml')->__($label)
        );

        unset($params['label']);

        $options = array_merge($options, $params);

        $this->addColumn($id, $options);

        $this->_fieldValues[$id] = $values->toArray();

        return $this;
    }

    /**
     * @param $id
     * @param $label
     * @param Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface $values
     * @param array $params
     * @return $this
     */
    public function addMultiSelectColumn($id, $label, Avestique_Extendcore_Model_Adminhtml_System_Config_Source_Interface $values, $params = array())
    {
        $this->addSelectColumn($id, $label, $values, $params);

        $this->_selectBox[$id] = true;

        return $this;
    }

    /**
     * @param string $columnName
     * @return string
     * @throws Exception
     */
    protected function _renderCellTemplate($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new Exception('Wrong column name specified.');
        }

        if (!isset($this->_fieldValues[$columnName])) {
            throw new Exception('Wrong column type specified.');
        }

        $column     = $this->_columns[$columnName];

        $values     = $this->_fieldValues[$columnName];

        $inputName  = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']'.(isset($this->_selectBox[$columnName]) ? '[]' : '');

        $rendered = '<select '.(isset($this->_selectBox[$columnName])?'multiple':'').' '.($column['size'] ? 'size="' . $column['size'] . '"' : '').' class="'.(isset($column['class']) ? $column['class'] : 'input-text').'" '.(isset($column['style']) ? ' style="'.$column['style'] . '"' : '').(empty($column['width'])? ' ' : 'style="width:'.$column['width'].'!important;" ' ).' name="'.$inputName.'">';

        foreach($values as $_option => $_label)
        {
            $rendered .= '<option value="'.$_option.'">'.$_label.'</option>';
        }

        $rendered .= '</select>';

        return $rendered;
    }
}